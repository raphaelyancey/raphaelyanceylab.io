---
layout: page
title:  "Kévin le robot-guéridon"
categories: [ fr ]
tags: [ arduino, plastic arts, sensors ]
---

#### Rencontre du troisième type avec le cousin art déco de R2D2

Réalisé sur la plateforme [Arduino](http://arduino.cc), Kévin est un petit robot très attachant :

* Il gazouille grâce à un petit **haut-parleur** amplifié avec un gobelet
* Il hérisse ses cheveux nonchalamment grâce à un petit **ventilateur** fixé au sommet de sa charpente en **carton-plume**
* Il se déplace comme il peut — souvent aléatoirement — en vibrant, grâce à un **moteur** auquel un poids légèrement désaxé est fixé
* Il réagit au interactions extérieures grâce à un **détecteur de distance** à ultrasons et un **micro**

<video muted autoplay loop>
    <source src="{{ "/assets/img/kevin/video1.mp4" | relative_url }}" type="video/mp4">
    <source src="{{ "/assets/img/kevin/video1.webm" | relative_url }}" type="video/webm">
</video>

<video muted autoplay loop>
    <source src="{{ "/assets/img/kevin/video2.mp4" | relative_url }}" type="video/mp4">
    <source src="{{ "/assets/img/kevin/video2.webm" | relative_url }}" type="video/webm">
</video>

<iframe width="640" height="360" src="https://www.youtube-nocookie.com/embed/Cn-HpssQhAU" frameborder="0" allowfullscreen></iframe>

<br>

**The Arduino script in his heart** ([source](https://gist.github.com/raphaelyancey/6af052d624eeb27ec204cdad2c27f6b3))

<script src="https://gist.github.com/raphaelyancey/6af052d624eeb27ec204cdad2c27f6b3.js"></script>

<br>

{% picture /assets/img/kevin/proto.jpg --alt Prototype %}

<small>*Réalisé avec [Arthur Baude](http://arthurbaude.com), Yoann T. et François dans le cadre du weekend créatif Culture Expérience Days 2016. Extraits vidéo: [Week-end créatif - Culture Experience Days - 18.19.20 mars 2016](https://vimeo.com/161938666)*</small>