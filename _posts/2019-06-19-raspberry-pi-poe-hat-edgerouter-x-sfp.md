---
layout: post
title:  "Raspberry Pi PoE hat with the EdgeRouter X-SFP"
description: "Why the hat won't work."
date:   2019-06-19 16:13:00 +0200
categories: [ en ]
tags: [ hardware ]
---

Might be overkill to write a blog post about it but it can save you some time and money if you're willing to power you Pi with the Ubiquiti EdgeRouter X-SFP.

- The official Raspberry Pi PoE hat supports the 802.3af PoE, as known as "active" PoE, operating on a **48V** voltage. The packaging confirms that by specifying that the hat supports 36-56V.
- The EdgeRouter X-SFP only supports *passive*, **24V** PoE output.

Thus, **the hat cannot be powered via passive 24V PoE**.