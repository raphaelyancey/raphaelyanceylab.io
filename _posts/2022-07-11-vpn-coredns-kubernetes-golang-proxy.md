---
layout: post
title:  "Blocked Kubernetes DNS queries when working on a VPN"
date:   2022-07-11 18:17:00 +0200
categories: [ en ]
tags: [ programming, kubernetes, dns ]
---

If you're using a full-traffic VPN on your development machine, are using Kubernetes (or `k3d`, `k3s` and such) and Go language, you might
have seen that some `proxy.golang.org` requests are blocked when building your image and executing `go get`.

**TLDR;** patch `coredns`'s `ConfigMap` to use DoT (DNS over TLS) instead of its own `resolv.conf`.

`coredns.yaml`
```yaml
- op: replace
  path: "/data/Corefile"
  value: |
    .:53 {
      errors
      health
      ready
      kubernetes cluster.local in-addr.arpa ip6.arpa {
        pods insecure
        fallthrough in-addr.arpa ip6.arpa
      }
      hosts /etc/coredns/NodeHosts {
        ttl 60
        reload 15s
        fallthrough
      }
      prometheus :9153
      forward . tls://1.1.1.1 tls://1.0.0.1 {
        tls_servername cloudflare-dns.com
        health_check 5s
      }
      cache 30
      loop
      reload
      loadbalance
    }
```

`patch-coredns.sh`
```bash
#!/bin/bash
KUBECONFIG= kubectl patch cm -n kube-system coredns --patch-file ~/path/to/coredns.yaml --type json
KUBECONFIG= kubectl -n kube-system rollout restart deployment coredns
```

And then start your cluster with
```bash
k3d cluster start; ./patch-coredns.sh
```

A cleaner, more lasting solution would be the get the current `ConfigMap` and dynamically make the changes to replace `forward . /etc/resolv.conf` with the DoT lines.

**NOTE:** if the default `ConfigMap` changes, you'll have to reflect the changes in the replaced one.

---

This solution comes from [https://fizdoonk.medium.com/k3d-and-blocked-dns-5331638c98d0](https://fizdoonk.medium.com/k3d-and-blocked-dns-5331638c98d0), I just made the patch easier to apply.
